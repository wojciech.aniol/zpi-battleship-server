const SHIP_NUM = 5;
module.exports = ({ name }) => {
  const members = new Map();
  let roomHistory = [];
  const playersBoard = {};
  const playersScore = {};
  const playersShots = {};

  function broadcastMessage(message) {
    console.log('emit message');
    members.forEach((m) => {
      m.emit('message', message);
    });
  }

  function addEntry(entry) {
    roomHistory = roomHistory.concat(entry);
  }

  function setPlayerBoard(player, board, params) {
    console.log(`player: ${player} set board`);
    playersBoard[player] = board;
    playersScore[player] = 0;
    playersShots[player] = 0;
    const createEntry = () => ({ user: params.user, event: 'rozstawił/a swoje statki!' });
    const entry = { ...createEntry() };

    members.forEach((m) => {
      m.emit('message', { room: params.room, ...entry });
    });

    if (members.size === Object.keys(playersBoard).length && members.size === 2 && Object.keys(playersBoard).length === 2) {
      members.forEach((m) => {
        console.log(`gracz: ${m}`);
        m.emit('gameReady', true);
      });
      members.get(player).emit('yourTurn');
    }
  }

  function checkTorpedo(player, cords, callback, db, getUserEmail) {
    console.log('checkTorpedo triggered', player, cords);
    // eslint-disable-next-line array-callback-return
    Object.keys(playersBoard).map((p) => {
      if (p !== player) {
        console.log(playersBoard[p][cords[0]][cords[1]]);
        callback(playersBoard[p][cords[0]][cords[1]]);
        playersScore[player] += playersBoard[p][cords[0]][cords[1]];
        playersShots[player] += 1;
        console.log('player score: ', playersScore[player]);
        if (playersScore[player] === SHIP_NUM) {
          members.get(player).emit('gameFinished', { youWon: true });
          members.get(p).emit('gameFinished', { youWon: false });
          const winnerEmail = getUserEmail(player);
          const looserEmail = getUserEmail(p);
          let currWinnerPoints;
          let currLooserPoints;
          let winnerWins;
          let winnerBattles;
          let looserBattles;
          // Algorytm
          const winnerPointsCalc = () => {
            let rankDiff = currWinnerPoints - currLooserPoints;
            const hitDiff = playersScore[player] - playersScore[p];
            const hitMissDiff = playersScore[player] - (playersShots[player] - playersScore[player]);
            if (rankDiff > -50 && rankDiff < 50) {
              rankDiff = 15;
            } else if (rankDiff >= 50) {
              rankDiff = 800 / Math.abs(rankDiff);
            } else {
              rankDiff = Math.abs(rankDiff) / 3;
            }
            const result = Math.round((rankDiff / 2 + hitDiff * 2 + hitMissDiff) / 2);
            if (result > 50) {
              return 50;
            }
            return result;
          };
          const looserPointsCalc = () => {
            let rankDiff = currLooserPoints - currWinnerPoints;
            const hitDiff = playersScore[p] - playersScore[player];
            const hitMissDiff = playersScore[p] - (playersShots[p] - playersScore[p]);
            if (rankDiff > -50 && rankDiff < 50) {
              rankDiff = -15;
            } else if (rankDiff >= 50) {
              rankDiff = -Math.abs(rankDiff) / 3;
            } else {
              rankDiff = -800 / Math.abs(rankDiff);
            }
            const result = Math.round((rankDiff / 2 + hitDiff * 2 + hitMissDiff) / 2);
            if (result < -50) {
              return -50;
            }
            return result;
          };
          const date = new Date();
          // eslint-disable-next-line prefer-template
          const id_date = `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)} ${('0' + date.getHours()).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}:${('0' + date.getSeconds()).slice(-2)}.${date.getMilliseconds()}`;
          // Update database
          Promise.all([
            db.query('SELECT * FROM player WHERE email = $1', [winnerEmail]).then((res) => {
              currWinnerPoints = res.rows[0].points;
              winnerWins = res.rows[0].wins;
              winnerBattles = res.rows[0].battles;
            }).catch((err) => console.log('erorr: SELECT points FROM player WHERE email: ', err)),
            db.query('SELECT * FROM player WHERE email = $1', [looserEmail]).then((res) => {
              currLooserPoints = res.rows[0].points;
              looserBattles = res.rows[0].battles;
            }).catch((err) => console.log('erorr: SELECT points FROM player WHERE email: ', err)),
          ])
            .then(() => {
              let newWinnerPoints = currWinnerPoints + winnerPointsCalc();
              let newLooserPoints = currLooserPoints + looserPointsCalc();
              if (newWinnerPoints > 5000) {
                newWinnerPoints = 5000;
              }
              if (newLooserPoints < 500) {
                newLooserPoints = 500;
              }
              console.log(`winner ${winnerEmail} points: ${winnerPointsCalc()}`);
              db.query('UPDATE player SET points = $1, wins = $2, battles = $3 WHERE email = $4', [newWinnerPoints, winnerWins + 1, winnerBattles + 1, winnerEmail], (_err) => {
                if (_err) console.log('error: UPDATE player SET points:', _err);
              });
              console.log(`looser ${looserEmail} points: ${looserPointsCalc()}`);
              db.query('UPDATE player SET points = $1, battles = $2 WHERE email = $3', [newLooserPoints, looserBattles + 1, looserEmail], (_err) => {
                if (_err) console.log('error: UPDATE player SET points:', _err);
              });
              console.log(`adding battle ${id_date} into the history`);
              db.query('INSERT INTO battle (id_date, winner, looser, winner_rank, looser_rank, winner_points, looser_points, winner_hits, looser_hits, winner_shots, looser_shots) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)',
                [id_date, winnerEmail, looserEmail, currWinnerPoints, currLooserPoints, winnerPointsCalc(), looserPointsCalc(), playersScore[player], playersScore[p], playersShots[player], playersShots[p]], (err) => {
                  if (err) {
                    throw err;
                  }
                });
            });
        }
        members.get(p).emit('yourTurn', cords);
      }
    });
  }

  function getRoomHistory() {
    return roomHistory.slice();
  }

  function addUser(client) {
    members.set(client.id, client);
  }

  function removeUser(client) {
    console.log('removeUser', client);
    members.delete(client);
  }

  function isRoomAvailable() {
    return members.size < 2;
  }

  function serialize() {
    return {
      name,
      numMembers: members.size,
    };
  }

  return {
    broadcastMessage,
    addEntry,
    setPlayerBoard,
    getRoomHistory,
    addUser,
    removeUser,
    serialize,
    checkTorpedo,
    isRoomAvailable,
  };
};

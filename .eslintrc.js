module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    "import/no-named-as-default": 0,
    camelcase: 0,
    "react/destructuring-assignment": 0,
    "no-console": 0,
    "max-len": 0,
    "jsx-a11y/click-events-have-key-events": 0,
    "jsx-a11y/no-static-element-interactions": 0,
    "no-alert": 0,
    "no-plusplus": 0,
    "linebreak-style": 0
  },
};

function makeHandleEvent(client, clientManager, roomManager) {
  function ensureExists(getter, rejectionMessage) {
    return new Promise((resolve, reject) => {
      const res = getter();
      return res ? resolve(res) : reject(rejectionMessage);
    });
  }

  function ensureUserSelected(clientId) {
    return ensureExists(
      () => clientManager.getUserByClientId(clientId),
      'select user first',
    );
  }

  function ensureValidRoom(roomName) {
    return ensureExists(
      () => roomManager.getRoomByName(roomName),
      `invalid room name: ${roomName}`,
    );
  }

  function ensureValidRoomAndUserSelected(roomName) {
    return Promise.all([
      ensureValidRoom(roomName),
      ensureUserSelected(client.id),
    ]).then(([room, user]) => Promise.resolve({ room, user }));
  }

  function handleEvent(roomName, createEntry) {
    return ensureValidRoomAndUserSelected(roomName).then(({ room, user }) => {
      // append event to room history
      const entry = { user, ...createEntry() };
      console.log('user:', user);
      console.log('entry:', entry);
      room.addEntry(entry);

      // notify other clients in room
      room.broadcastMessage({ room: roomName, ...entry });
      return room;
    });
  }

  function isRoomValid(roomName) {
    console.log(client.id, roomName);
    return ensureValidRoomAndUserSelected(roomName).then(({ room, user }) => ({ room, user }));
  }

  return {
    handleEvent,
    isRoomValid,
  };
}

module.exports = (client, clientManager, roomManager, db) => {
  const { handleEvent, isRoomValid } = makeHandleEvent(client, clientManager, roomManager);

  function handleNewUser(userName, callback) {
    console.log('handleNewUser: ', userName);
    if (!clientManager.isUserAvailable(userName)) {
      console.log('handleNewUser: ', userName, ' - user is not available');
      return callback('user is not available');
    }
    clientManager.newUser(userName);
    clientManager.registerClient(client, userName);

    // db.query('SELECT * FROM player WHERE email = $1', [userName], (err, row) => {
    //   if (err) {
    //     console.log('error occur: ', err);
    //     // throw err;
    //   } else if (!row) {
    //     console.log('handleNewUser insert into database ', userName);
    //     db.query('INSERT INTO player (email, points) VALUES ($1, 1000)', [userName], () => {
    //       if (err) {
    //         throw err;
    //       }
    //     });
    //   }
    // });

    return callback(null, userName);
  }

  function handleCreateRoom(name, callback) {
    roomManager.createRoom(name);
    clientManager.clients.forEach((value) => {
      // value.client.emit('newroom');
      value.client.emit('roomsUpdated');
    });
    return callback(null, name);
  }

  function handleRegister(userName, callback) {
    if (!clientManager.isUserAvailable(userName)) {
      return callback('user is not available');
    }

    const user = clientManager.getUserByName(userName);
    clientManager.registerClient(client, user);

    return callback(null, user);
  }

  function handleJoin(roomName, callback) {
    isRoomValid(roomName).then(({ room, user }) => {
      if (room.isRoomAvailable()) {
        console.log('adding user to database...', user);
        db.query('SELECT * FROM player WHERE email = $1', [user], (err, results) => {
          console.log('row:', results.rows);
          if (err) {
            console.log('error occur: ', err);
            // throw err;
          } else if (results.rows.length === 0) {
            console.log('handleNewUser insert into database ', user);
            db.query('INSERT INTO player (email, points) VALUES ($1, 1000)', [user], () => {
              if (err) {
                throw err;
              }
            });
          } else {
            console.log('user exists.');
          }
        });
        room.addUser(client);
        clientManager.clients.forEach((value) => {
          value.client.emit('roomsUpdated');
        });
        callback(null, room.getRoomHistory());
        const createEntry = () => ({ event: `dołączył do ${roomName}` });
        handleEvent(roomName, createEntry);
      } else {
        callback('Pokój pełny!');
      }
    }).catch((err) => {
      console.log('err:', roomName, err);
      return callback(`Wystąpił błąd podczas próby połączenia: ${err}`);
    });
  }

  function handleLeave(roomName, callback) {
    const createEntry = () => ({ event: `opuścił pokój ${roomName}` });

    handleEvent(roomName, createEntry)
      .then((room) => {
        // remove member from room
        room.removeUser(client.id);
        clientManager.clients.forEach((value) => {
          value.client.emit('roomsUpdated');
        });

        callback(null);
      })
      .catch(callback);
  }

  function handleMessage({ roomName, message }, callback) {
    const createEntry = () => ({ message });

    handleEvent(roomName, createEntry)
      .then(() => callback(null))
      .catch(callback);
  }
  function handleBoard(params, callback) {
    isRoomValid(params.roomName).then(({ room }) => {
      room.setPlayerBoard(client.id, params.board, { room: params.roomName, user: clientManager.getUserByClientId(client.id) });
      callback();
    });
  }

  function handleTorpedo(params, callback) {
    isRoomValid(params.roomName).then(({ room }) => {
      room.checkTorpedo(client.id, params.cords, callback, db, (id) => clientManager.getUserByClientId(id));
      const createEntry = () => ({ event: `wystrzelił w ${JSON.stringify(params.cords)}` });
      handleEvent(params.roomName, createEntry);
    });
  }

  // function handleBoard({ roomName, userBoard }, callback) {
  //   const createEntry = () => ({ userBoard });

  //   handleEvent(roomName, createEntry)
  //     .then(() => callback(null))
  //     .catch(callback);
  // }

  /*
  //handleBattlePoints(player_rank, opponent_rank, time, player_hits, opponent_hits, player_misses, winner(0-yes, 1-no))
  function handleBattlePoints(p1Rank, p2Rank, time, p1Hits, p2Hits, p1Misses, win) {
    let rankDiff = p1Rank - p2Rank;
    let hitDiff = p1Hits - p2Hits;
    let hitMissDiff = p1Hits - p1Misses;

    if (win === 1) {
      if (rankDiff > -50 && rankDiff < 50) {
        rankDiff = 15;
      } else if (rankDiff >= 50) {
        rankDiff = 800/Math.abs(p1Rank-p2Rank);
      } else {
        rankDiff = Math.abs(p1Rank-p2Rank)/3;
      }
    } else if (win === 0) {
      if (rankDiff > -50 && rankDiff < 50) {
        rankDiff = -15;
      } else if (rankDiff >= 50) {
        rankDiff = -Math.abs(p1Rank-p2Rank)/3;
      } else {
        rankDiff = -800/Math.abs(p1Rank-p2Rank);
      }
    } else {
      console.log("Error - points algorythm");
      return 0;
    }

    let result = Math.round((rankDiff/2 + hitDiff*2 + hitMissDiff)/time*150);

    if (result > 50) {
      return 50;
    } else if (result < -50) {
      return -50;
    } else {
      return result;
    }
  }

  function handleBattleDatabaseUpdate(player1, player2, time, winner, player1Hits, player2Hits, player1Misses, player2Misses, player1Field, player2Field, callback) {
    let player1Rank = db.get(`SELECT points FROM player WHERE email='${player1}'`, (err, row) => {
      if (err) {
        throw err;
      } else {
        return row.points;
      }
    });
    let player2Rank = db.get(`SELECT points FROM player WHERE email='${player2}'`, (err, row) => {
      if (err) {
        throw err;
      } else {
        return row.points;
      }
    });

    let player1Points;
    let player2Points;
    if (winner === 0) {
      player1Points = handleBattlePoints(player1Rank, player2Rank, time, player1Hits, player2Hits, player1Misses, 1);
      player2Points = handleBattlePoints(player2Rank, player1Rank, time, player2Hits, player1Hits, player2Misses, 0);
    } else if (winner === 1) {
      player1Points = handleBattlePoints(player1Rank, player2Rank, time, player1Hits, player2Hits, player1Misses, 0);
      player2Points = handleBattlePoints(player2Rank, player1Rank, time, player2Hits, player1Hits, player2Misses, 1);
    }

    let player1NewRank = player1Rank + player1Points;
    let player2NewRank = player2Rank + player2Points;
    if (player1NewRank < 500) {
      player1NewRank = 500;
    } else if (player1NewRank > 5000) {
      player1NewRank = 5000;
    }
    if (player2NewRank < 500) {
      player2NewRank = 500;
    } else if (player2NewRank > 5000) {
      player2NewRank = 5000;
    }

    db.run(`UPDATE user SET points = ${player1NewRank} WHERE email = '${player1}'`, (err) => {
      if (err) {
        throw err;
      }
    });
    db.run(`UPDATE user SET points = ${player2NewRank} WHERE email = '${player2}'`, (err) => {
      if (err) {
        throw err;
      }
    });
    db.run(`INSERT INTO battle (player1, player2, player1_rank, player2_rank,
            player1_points, player2_points, winner, time, player1_hits, player2_hits,
            player1_misses, player2_misses, player1_field, player2_field)
            VALUES ('${player1}', '${player2}', ${player1Rank}, ${player2Rank},
            ${player1Points}, ${player2Points}, ${winner}, ${time}, ${player1Hits}, ${player2Hits},
            ${player1Misses}, ${player2Misses}, '${player1Field}', '${player2Field}')`, (err) => {
      if (err) {
        throw err;
      }
    });

    //return callback(); ???
  }
  */

  function handleGetRooms(_, callback) {
    return callback(null, roomManager.serializeRooms());
  }

  function handleGetAvailableUsers(_, callback) {
    return callback(null, clientManager.getAvailableUsers());
  }

  function handleDisconnect() {
    // remove user profile
    clientManager.removeClient(client);
    // remove member from all rooms
    roomManager.removeClient(client);
  }

  return {
    handleRegister,
    handleNewUser,
    handleCreateRoom,
    handleJoin,
    handleLeave,
    handleMessage,
    handleGetRooms,
    handleGetAvailableUsers,
    handleDisconnect,
    handleBoard,
    handleTorpedo,
  };
};
